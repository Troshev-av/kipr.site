Vue.component('menu-component', {
    components: {
        'menu-link': menuLinkComponent,
        'menu-button': menuButtonComponent,
        'menu-collapse': menuCollapseComponent
    },
    data: function () {
        return {
            menu: []
        };
    },
    mounted: function () {
        requestApi
            .get('menu')
            .then(response => (this.menu = response.data))
    },
    methods: {
        closeMenu: function () {
            this.$emit('menu-close');
        }
    }
});
