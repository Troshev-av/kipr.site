var menuLinkComponent = {
    template: `
        <a
            :class="['menu-link', 'menu-item--title', {'is-disabled': this.isDisabled}]"
            :href="this.aviableLink">
            <i
                :class="['menu-item--icon', this.iconClass]">
            </i>
            <span class="menu-item--text">{{this.label}}</span>
            <el-badge
                class="menu-item--badge"
                v-if="this.notify > 0"
                :value="this.notify"
            ></el-badge>
        </a>
    `,
    props: {
        isActive: {
            type: Boolean,
            default: false,
        },
        isDisabled: {
            type: Boolean,
            default: false,
        },
        href: String,
        icon: String,
        label: {
            type: String,
            default: '',
            required: true,
        },
        notify: {
            type: Number,
            default: 0,
        }
    },
    computed: {
        iconClass: function () {
            return 'icon-' + this.icon + (!this.isActive ? '-outline' : '');
        },
        aviableLink: function () {
            return !this.isDisabled ? this.href : '#';
        }
    }
};
