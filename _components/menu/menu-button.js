var menuButtonComponent = {
    template: `
        <a
            class="menu-button--area"
            :href="this.aviableLink">
            <el-button
                type="primary"
                class="menu-button"
                :disabled="this.isDisabled">
                <i
                    :class="['menu-item--icon', this.iconClass]">
                </i>
                <span class="menu-item--text">{{this.label}}</span>
            </el-button>
        </a>
    `,
    props: {
        isDisabled: {
            type: Boolean,
            default: false,
        },
        href: String,
        icon: String,
        label: {
            type: String,
            default: '',
            required: true,
        }
    },
    computed: {
        iconClass: function () {
            return 'icon-' + this.icon;
        },
        aviableLink: function () {
            return !this.isDisabled ? this.href : '#';
        }
    }
};
