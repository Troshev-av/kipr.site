var menuCollapseComponent = {
    template: `
        <div class="menu-collapse">
            <div
                :class="['menu-item--title', 'menu-collapse--title', {'is-open': this.isOpen}, {'is-active': this.isActive}]"
                @click="toogle">
                <el-badge
                    class="menu-item--badge is-dot"
                    is-dot
                    v-if="this.notify > 0">
                    <i
                        :class="['menu-item--icon', this.iconClass]">
                    </i>
                </el-badge>
                <i
                    v-else
                    :class="['menu-item--icon', this.iconClass]">
                </i>
                <span class="menu-item--text">{{this.label}}</span>
                <i class="el-icon-arrow-left"></i>
            </div>
            <el-collapse-transition>
                <div
                    class="menu-collapse--list"
                    v-show="this.isOpen">
                    <a
                        v-for="subitem in this.items"
                        :class="['menu-link', 'menu-item--subitem', {'is-active': subitem.isActive}, {'is-disabled': subitem.isDisabled}]"
                        :href="getAviableLink(subitem)">
                        <span class="menu-item--text">{{subitem.label}}</span>
                        <el-badge
                            class="menu-item--badge"
                            v-if="subitem.notify > 0"
                            :value="subitem.notify"
                        ></el-badge>
                    </a>
                </div>
            </el-collapse-transition>
        </div>
    `,
    data: function () {
        var activeChild = this.items.some(function (item) {
            return item.isActive;
        });

        return {
            isOpen: activeChild,
            isActive: activeChild
        };
    },
    props: {
        icon: String,
        label: {
            type: String,
            default: '',
            required: true,
        },
        items: Array
    },
    computed: {
        iconClass: function () {
            return 'icon-' + this.icon + (!this.isActive ? '-outline' : '');
        },
        notify: function () {
            return this.items.reduce(function(sum, item) {
              return sum + item.notify;
            }, 0);
        }
    },
    methods: {
        getAviableLink: function (item) {
            return !item.isDisabled ? item.href : '#';
        },
        toogle: function () {
            this.isOpen = !this.isOpen;
        }
    }
};
