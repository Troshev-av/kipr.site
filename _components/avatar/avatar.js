Vue.component('avatar', {
    template: `
        <a
            :href="link" class="avatar"
            :style="{
                height: pixelSize,
                width: pixelSize,
                fontSize: textSize + 'px',
                borderRadius: size / 2 + 'px',
                cursor: link ? 'pointer' : '',
                backgroundImage: 'url(' + image + ')'
            }">
            {{image ? '' : initials}}
        </a>
    `,
    props: {
        link: {
            type: String,
            default: null,
        },
        name: {
            type: String,
            default: '',
            required: true,
        },
        image: {
            type: String,
            default: '',
        },
        size: {
            type: Number,
            default: 36,
        },
        textSize: {
            type: Number,
            default: 14,
        }
    },
    computed: {
        pixelSize: function () {
            return this.size + 'px';
        },
        initials: function () {
            return this.name.split(' ', 2).reduce(function(initials, word) {
              return initials + ((word[0]) ? word[0] : '');
            }, '');
        }
    }
});
