Vue.component('login', {
    data: function () {
        return {
            loginModel: {
                login: '',
                password: ''
            },
            rules: {
                login: {required: true, message: 'Логин не должн быть пустым', trigger: 'blur'},
                password: {required: true, message: 'Пароль не должн быть пустым', trigger: 'blur'}
            },
            inputPasswordType: 'password'

        };
    },
    computed: {
        showPasswordDisabled: function () {
            return !(this.loginModel.password.length > 0);
        }
    },
    methods: {
        submit: function () {
            this.loginModel.login = '';
            this.$refs.loginForm.validate((valid) => {
                if (valid) {
                    this.auth();
                } else {
                    this.showError('Заполните все необходимые поля');
                }
            });
        },
        auth: function () {
            requestApi
                .get('login', {
                    params: this.loginModel
                })
                .then((response) => {
                    if (response.data.login) {
                        window.location.href = '/';
                    } else {
                        this.showError('Данного пользователя не существует.');
                        this.$refs.loginForm.resetFields();
                    }
                });
        },
        showError: function (message) {
            this.$message({
                showClose: true,
                message: message,
                type: 'error'
            });
        },
        showPassword: function () {
            this.inputPasswordType = 'text';
        },
        hidePassword: function () {
            this.inputPasswordType = 'password';
        }
    }
});
