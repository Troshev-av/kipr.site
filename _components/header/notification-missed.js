var notificationMissed = {
    template: `
        <li class="notification">
            <div class="notification-icon notification-icon-missed">
                <i class="icon-warning"></i>
            </div>

            <div class="notification-content">
                <div class="notification-title color-black">Пропущенное обучение</div>
                <div class="notification-text color-gray">
                    <span>Вы не прошли назначенное Вам </span>
                    <a
                        class="link-default"
                        :href="href">
                        обучение
                    </a>
                </div>
            </div>
        </li>
    `,
    props: {
        href: {
            type: String,
            required: true
        }
    }
};
