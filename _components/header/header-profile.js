var headerProfileComponent = {
    data: function () {
        return {
            user: {}
        };
    },
    mounted: function () {
        requestApi
            .get('user')
            .then(response => (this.user = response.data))
    }
};
