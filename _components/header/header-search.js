var headerSearchComponent = {
    template: `
        <el-autocomplete
            placeholder="Поиск"
            popper-class="header-search--popper"
            :min="3"
            v-model="value"
            :fetch-suggestions="querySearch"
            ref="input"
            @select="selectItem">
            <i @click="focusInput" slot="prefix" class="icon-search header-search--icon"></i>
            <template slot-scope="{item}">
                <div class="header-search--type">
                    <i v-if="item.type === 'news'" slot="prefix" class="icon-news color-silver"></i>
                    <i v-else-if="item.type === 'materials'" slot="prefix" class="icon-materials color-silver"></i>
                </div>
                <div class="header-search--item">
                    <div class="header-search--title color-black">{{item.title}}</div>
                    <div class="header-search--description color-gray">{{item.description}}</div>
                </div>
            </template>
        </el-autocomplete>
    `,
    data: function () {
        return {
            value: ''
        };
    },
    props: {
        isFocus: {
            type: Boolean,
            default: false,
        },
    },
    watch: {
        isFocus: function (val, oldVal) {
            if (val) {
                this.focusInput();
            }
        }
    },
    methods: {
        focusInput: function () {
            this.$refs.input.focus();
        },

        querySearch: function (queryString, callback) {
            var returnEmpty = function () {
                callback([]);
            };

            if (queryString && queryString.trim().length > 2) {
                requestApi
                    .get('search', {
                        params: {
                            search: queryString
                        }
                    })
                    // .then(response => (callback(response.data)))
                    .catch(error => (returnEmpty()))
            } else {
                returnEmpty();
            }
        },

        selectItem: function (item) {
            var href;
            switch (item.type) {
                case 'materials':
                    href = '/materials/' + item.id;
                    break;
                case 'news':
                    href = '/news/' + item.id;
                    break;
                default:
                href = '#';
            }
            window.location.href = href;
        }
    }
};
