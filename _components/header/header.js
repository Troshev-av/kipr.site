Vue.component('header-component', {
    data: function () {
        return {
            showSearchBar: false
        }
    },
    components: {
        'header-profile': headerProfileComponent,
        'header-search': headerSearchComponent,
        'header-notifications': headerNotificationsComponent
    },
    methods: {
        openSearchBar: function () {
            this.showSearchBar = true;
            this.$refs.search.focusInput();
        },
        hideSearchBar: function () {
            this.showSearchBar = false;
        },
        openMenu: function () {
            this.$emit('menu-open');
        }
    }
});
