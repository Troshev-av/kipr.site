var notificationTraining = {
    template: `
        <li class="notification">
            <div class="notification-icon notification-icon-training">
                <i class="icon-materials"></i>
            </div>

            <div class="notification-content">
                <div class="notification-title color-black">Новое обучение</div>
                <div class="notification-text color-gray">
                    <span>Вам назначено новое</span>
                    <a class="link-default" :href="href">обучение</a>
                    <span v-if="expire">. Успейте пройти его до {{expire}}</span>
                </div>
            </div>
        </li>
    `,
    props: {
        href: {
            type: String,
            required: true
        },
        expire: {
            type: String,
            default: ''
        }
    }
};
