var notificationAnswer = {
    template: `
        <li class="notification">
            <div class="notification-icon notification-icon-answer">
                <avatar
                    :name="user.name"
                    :image="user.avatar"
                    :size="30"
                    :textSize="12"
                ></avatar>
            </div>

            <div class="notification-content">
                <div class="notification-title color-black">Ответ на комментарий</div>
                <div class="notification-text color-gray">
                    <span>{{user.name}}</span>
                    <a
                        class="link-default"
                        :href="href">
                        ответил<template v-if="user.gender == 'female'">а</template>
                    </a>
                    <span> на Ваш комментарий</span>
                </div>
            </div>
        </li>
    `,
    props: {
        href: {
            type: String,
            required: true
        },
        user: {
            type: Object,
            default: {}
        }
    }
};
