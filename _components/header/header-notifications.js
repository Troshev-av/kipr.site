var headerNotificationsComponent = {
    components: {
        'notification-training': notificationTraining,
        'notification-answer': notificationAnswer,
        'notification-missed': notificationMissed
    },
    template: `
        <el-popover
            popper-class="notifications-popper"
            placement="bottom"
            width="350"
            trigger="click"
            @hide="readNotifications">
            <div class="notifications-header">
                <span v-if="notifications.length > 0" class="notifications-title notifications-title-solid color-black">Новые уведомления</span>
                <span v-else class="notifications-title color-gray">Нет новых уведомлений</span>
            </div>

            <div v-if="isLoading" class="notifications-loading">
                <i class="el-icon-loading"></i>
            </div>

            <ul v-else class="notifications-list">
                <component
                    v-for="notify in notifications"
                    v-bind="notify"
                    :key="notify.id"
                    v-bind:is="'notification-' + notify.type"
                    :type="null">
                </component>
            </ul>

            <div class="notifications-footer">
                <a href="/notifications" class="color-black notifications-footer--link">Показать все</a>
            </div>

            <el-badge
                slot="reference"
                is-dot
                :class="['header-badge', {'is-hidden': !badge}]">
                <el-button
                    class="header-button color-gray"
                    icon="icon-notification">
                </el-button>
            </el-badge>
        </el-popover>
    `,
    data: function () {
        return {
            notifications: [],
            badge: false,
            isLoading: true
        };
    },
    mounted: function () {
        $self = this;
        requestApi
            .get('notifications')
            .then(function (response) {
                $self.notifications = response.data;
                $self.badge = true;
                $self.isLoading = false;
            });
    },
    methods: {
        readNotifications: function () {
            this.badge = false;
        }
    }
};
