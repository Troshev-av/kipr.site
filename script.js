ELEMENT.locale(ELEMENT.lang.ruRU);

var vm;

var requestApi = axios.create({
    baseURL: '/kipr.site/_api/',
    timeout: 3000
});

// NOTE: AXIOS request pre-handler
requestApi.interceptors.request.use(function (config) {
    // NOTE: DEV-ONLY:
    config.url = config.url + '.json';
    console.log('Config ' + config.url + '     ->    ', config);
    return config;

}, function (error) {
    vm.$notify.error({
      title: 'Ошибка отправки',
      message: 'Перезагрузите страницу или попробуйте позже'
    });
    console.log('Ошибка отравки', error);
    return Promise.reject(error);
});

// NOTE: AXIOS response pre-handler
requestApi.interceptors.response.use(function (response) {
    console.log('Response ' + response.config.url + '     ->    ', response);
    return response;

}, function (error) {
    vm.$notify.error({
      title: 'Ошибка сервера',
      message: 'Сервер временно недоступен, попробуйте позже'
    });
    console.log('Ошибка получения', error);
    return Promise.reject(error);
});

// NOTE: Page error handler
window.onerror = function(error) {
    console.log('Ошибка страницы', error);
    vm.$notify.error({
      title: 'Ошибка',
      message: 'Перезагрузите страницу или попробуйте позже'
    });
};

// NOTE: Vue error handler
Vue.config.errorHandler = function (err, vm, info) {
    console.log('Ошибка Vue', err, vm, info);
    vm.$notify.error({
      title: 'Ошибка',
      message: 'Перезагрузите страницу или попробуйте позже'
    });
};

// NOTE: Vue warn handler. Working in dev mod only
Vue.config.warnHandler = function (msg, vm, trace) {
    console.error('Предупреждение Vue', msg, vm, trace);
    vm.$notify({
        title: 'Ошибка',
        message: 'Произошла ошибка, возможна некорректная работа сайта',
        type: 'warning'
    });
};


vm = new Vue ({
    el: '#app',
    data: function () {
        return {
            menuIsOpen: false
        };
    },
    computed: {
        width: function () {
            return window.innerWidth;
        }
    },
    methods: {
        openMenu: function () {
            this.menuIsOpen = true;
        },
        closeMenu: function () {
            this.menuIsOpen = false;
        }
    }
});
